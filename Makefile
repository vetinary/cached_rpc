PROJECT = cached_rpc

# .PHONY: release clean-release 

DEPS = resource_discovery sqlite3
dep_sqlite3 = https://github.com/maxlapshin/erlang-sqlite3.git HEAD
dep_resource_discovery = https://vetinary@bitbucket.org/vetinary/resource_discovery.git HEAD


include erlang.mk

