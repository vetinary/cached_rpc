% https://github.com/travis/erlang-uuid
-module(cached_rpc_storage_utils).
-export([uuid/0, uuid_to_string/1, uuid_get_parts/1]).

uuid() ->
    uuid(rnd(math:pow(2, 48)) - 1, rnd(math:pow(2, 12)) - 1, rnd(math:pow(2, 32)) - 1, rnd(math:pow(2, 30)) - 1).

rnd(F) ->
	random:uniform(round(F)).
	
uuid(R1, R2, R3, R4) ->
    <<R1:48, 4:4, R2:12, 2:2, R3:32, R4: 30>>.

uuid_to_string(U) ->
    lists:flatten(io_lib:format("~8.16.0b-~4.16.0b-~4.16.0b-~2.16.0b~2.16.0b-~12.16.0b", uuid_get_parts(U))).

uuid_get_parts(<<TL:32, TM:16, THV:16, CSR:8, CSL:8, N:48>>) ->
    [TL, TM, THV, CSR, CSL, N].