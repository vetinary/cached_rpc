-module(cached_rpc_storage).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------


-export([start_link/0]).
-export([store_message/1, get_message/1, get_message/0, clear_message/1]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

store_message(Msg) ->
    gen_server:call(?SERVER, {store_msg, Msg}).

get_message() ->
    gen_server:call(?SERVER, get_msg).

get_message(Uuid) ->
    gen_server:call(?SERVER, {get_msg, Uuid}).

clear_message(Uuid) ->
    gen_server:cast(?SERVER, {clear_msg, Uuid}).


%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init(_Args) ->
    sqlite3:open(rpc),
    ok = init_tables(sqlite3:list_tables(rpc)),
    sqlite3:close(rpc),
    {ok, []}.

handle_call({get_msg, Uuid}, _, State) ->
    Msg = get_msg(Uuid),
    {reply, Msg, State};

handle_call(get_msg, _, State) ->
    Msg = get_first_msg(),
    {reply, Msg, State};

handle_call({store_msg, Msg}, _, State) ->
    Uuid = store_msg(Msg),
    {reply, Uuid, State};

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast({clear_msg, Msg}, State) ->
    clear_msg(Msg),
    {noreply, State};

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

init_tables([]) ->
    MSG = [ {uuid, blob, [{primary_key, []}]},
            {args, blob} ],
    sqlite3:create_table(rpc, messages, MSG);
init_tables([_T|_H]) ->   
    ok.


store_msg(Msg) ->
    Uuid = cached_rpc_storage_utils:uuid(),
    Blob = erlang:term_to_binary(Msg),
    sqlite3:open(rpc),
    sqlite3:write(rpc, messages, [{uuid, blob(Uuid)}, {args, blob(Blob)}]),
    sqlite3:close(rpc),
    Uuid.

blob(V) when is_binary(V) ->
    {blob, V}.

get_msg(Uuid) ->
    sqlite3:open(rpc),
    Res = sqlite3:read(rpc, messages, {uuid, blob(Uuid)}),
    sqlite3:close(rpc),
    [{_, {blob, Res2}}] = rows(Res),
    Res4 = binary_to_term(Res2),
    Res4.

get_first_msg() ->
    sqlite3:open(rpc),
    Res = sqlite3:sql_exec(rpc, "select * from messages limit 1"),
    sqlite3:close(rpc),
    case rows(Res) of
    	[{{blob, Uuid}, {blob, Res2}}] -> 
			Res4 = binary_to_term(Res2),
            {Uuid, Res4};
    	[] -> [] 
	end.

clear_msg(Uuid) ->
    sqlite3:open(rpc),
    ok = sqlite3:delete(rpc, messages, {uuid, {blob, Uuid}}),
    sqlite3:close(rpc).

rows(SqlExecReply) ->
    case SqlExecReply of
        [{columns, _Columns}, {rows, Rows}] -> Rows;
        {error, Code, Reason} -> {error, Code, Reason}
    end.