-module(cached_rpc).
-behaviour(gen_server).
-define(SERVER, ?MODULE).
-define(INTERVAL, 5).
-define(LONG_INTERVAL, 1000).

-export([call/3]).

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-record(crpc_st, {timerid}).

call(M, F, A) -> 
	gen_server:cast(?SERVER, {call, M, F, A}).

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

init(_Args) ->
	{ok, #crpc_st{timerid=nil}}.

handle_call(_Request, _From, State) ->
    {reply, unknown, State}.

handle_cast({call, M, F, A}, State) ->
	Id = store_message(M, F, A),
	Tref = case rpc(M, F, A) of 
		true -> 
			remove_stored(Id), 			
			process_stored(State#crpc_st.timerid);
		_ -> delayed_process_stored(State#crpc_st.timerid)
	end,
	{noreply, State#crpc_st{timerid = Tref}};

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(process_stored, State) ->
	NewState = case read_and_process_msg(State#crpc_st.timerid) of 
		done -> State#crpc_st{ timerid = nil };
		true -> State#crpc_st{ timerid = process_stored(State#crpc_st.timerid) };
		NewTRef -> State#crpc_st{ timerid = NewTRef }
	end,
	{noreply, NewState};

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

read_and_process_msg(TimerId) -> 
	case read_stored() of
		[] -> done;
		{Id, [M, F, A]} ->
				case rpc(M, F, A) of 
					true -> remove_stored(Id);
					_ -> delayed_process_stored(TimerId)
				end
	end.

rpc(M, F, A) -> 
	case resource_discovery:fetch_resources(M) of
		error -> 
			error_logger:error_msg("error discovering resource for ~p (No connection?)", [M]),
			false;
		{ok, Nodes} -> 
			rpc(Nodes, M, F, A)
	end.

rpc([], _M, _F, _A) -> 
	true;

rpc([Node|Nodes], M, F, A) ->
	Res = rpc:call(Node, M, F, [node() | A]),
	case Res of 
		{badrpc, _Error} -> false;
		_ -> true == rpc(Nodes, M, F, A)
	end.

process_stored(TimerId) ->
	process_stored(TimerId, ?INTERVAL).

process_stored(TimerId, Interval) ->
	set_timeout(Interval, process_stored, TimerId).

delayed_process_stored(TimerId) ->
	set_timeout(?LONG_INTERVAL, process_stored, TimerId).

set_timeout(Interval, F, TimerId) ->
	cancel_timeout(TimerId),
	NewTRef = erlang:send_after(Interval, self(), F),
	NewTRef.

cancel_timeout(nil) -> true;
cancel_timeout(TRef) -> erlang:cancel_timer(TRef).

read_stored() ->
	cached_rpc_storage:get_message().

remove_stored(Id) ->
	cached_rpc_storage:clear_message(Id),
	true.

store_message(M, F, A) ->
	Uuid = cached_rpc_storage:store_message([M,F,A]),
	Uuid.

